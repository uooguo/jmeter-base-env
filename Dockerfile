FROM debian:latest
LABEL name = "lijy"
RUN apt-get update -y
ADD apache-jmeter /opt/jmeter/
ADD jre /opt/jre
RUN chmod 777 /opt/jmeter/bin/jmeter \
    && ln -s /opt/jmeter/bin/jmeter /usr/local/bin/jmeter \
    && chmod 777 /opt/jre/bin/java \
    && ln -s /opt/jre/bin/java /usr/local/bin/java
WORKDIR /workspace
CMD ["/bin/bash"]